# Python Training #1 Curriculum
## 1. Up-and-running with Python 3.x:
* How to install Python (for those who haven’t yet done so)
* Anaconda and scripts versus the interactive console
* Disperse code before, whether notebook or script
 
## 2. The basics:
* Print function, strings, input (10 minutes)
    * Math (5 minutes)
    * Variables (5 minutes)
	* Data structures (15 minutes)
		* List
			* One-d
			* Two-d
		* Tuple
		* Dict
		* Set
		* **Homework #1**: initialize three variables with integer values and store them in a list (5 minutes)
	* Data types (10 minutes)
	    * str
	    * int
	    * float
	    * list, dict, set, tuple
    * Loops (10-15 minutes)
        * For
        * While
    * Control statements (15 minutes)
        * If
		* Elif
        * Else
	* Functions (30 minutes)
		* Parameter-less 
		* With parameters
		* Return values
		* Useful Python built-ins
			* Int, float, str
			* Min, max
			* Range
			* Dir
			* Map, zip
			* type
		* **Homework #2**: Define a function that returns the largest odd number in a given list (10 minutes)
	* Scope (5 minutes)
		* Global versus local variables
	* Debugging (5 minutes)
		* Basics: EOL while scanning string literal, syntax error, indentation block
	* Classes (15 minutes)
		* Basic class definition (perhaps a simple calculator to demonstrate how to make a class)
		* “Shape” classes… Initialize rectangle with w, h
	* Comprehensions (20 minutes)
	    * List
	    * Set
	    * Dict
	    **Homework #3**: Initialize a list containing the numbers 1 through 10 using a comprehension (5 minutes) 
	* Importing modules (10 minutes)
		* Perhaps import `statistics` and run through some mean, median, sd problems
		* Showcase `os`, `urllib`, and some other powerful built-ins
			* Simple web-scraping exercise?

## 3. Data science applications
* pandas
	* Read in a file (csv, txt, xlsx, etc.)
	* Select columns (df[‘col’] versus df[[‘col1’, ‘col2’]], etc.)
	* Filtering (df[df[‘col’] == val])
	* Useful pandas DataFrame and Series methods: isnull(), notnull(), str methods
	* Assigning values/objects to columns (for feature engineering)
	* Pandas built-in plotting functions
* NumPy
	* Why NumPy is better than built-in Python lists at scale
	* Useful examples of numpy (basic matrix/array manipulation)
* Matplotlib(?)
* Scikit-learn basics:
	* Exploring all the different corners of sklearn (preprocessing, metrics, cluster, etc.)
	* Building a basic linear regression model (save the SVMs for later ☺)
		* Class instantiation
		* Fit, predict methods
		* (perhaps touch on `statsmodels`, too, as it’s much better for OLS analyses)
	* More detail:
		* OneHotEncoder versus pd.get_dummies()
		* Cross-validation, metrics modules, preprocessing scalers, etc.

## 4. How to search/ask a question when you're debugging
* Creating an effective Google query
* Searching Stack Overflow
