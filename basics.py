#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Feb 18 11:39:02 2017

@author: jacobmason
"""

#### The basics

### print function

# Printing a string
print("Hello, world!")

# printing special characters
print("Let's go to a \n new line.")

### Some math

## Basic operators
print(5 + 3)
print(5 - 3)
print(5 * 3)
print(5 / 3)

## Some more advanced operators
# floor division
print(5 // 3)

# absolute value
print(abs(-100))

# exponent
print(5 ** 2)

# modulo
print(11 % 2)

### Variables
## Numeric variables
x = 5
y = 10

# We can perform operations on numeric (e.g. int or float) variables, too, like addition:
x_plus_y = x + y

print("%d + %d equals %d" % (x, y, x_plus_y))

## String variables
s = 'I went to the store'

# We can index this string in a variety of ways:
print('The first five characters in "%s" are "%s".' % (s, s[:5]))

# Every second character
every_second_char = s[::2]

# Reversed string
my_string_rev = s[::-1]

# We can also perform "operations" on string variables:
other_s = ' and bought oranges.'

together_s = s + other_s

print('"%s" concatenated with "%s" is "%s"' % (s, other_s, together_s))

# String formatting
print('I want three decimal places: %.3f' % 3.3333333)
print('Just give me an integer: %d' % 3.333333)
print('How about two strings: %s and %s' % ('one', 'two'))

## Dynamic typing
# Python has dynamic typing, which means we can do things like this:
x = 'this is a string'
print(type(x))

x = 3.5
print(type(x))

from collections import Counter
x = Counter()

# You can also get user input using the `input` function
user_input = input("Enter a number: ")
print('Here\'s what you entered: %s' % user_input)

### Data structures
## lists
my_list = [1, 2, 3, 4, 5]

## dictionaries
phone_numbers = {'John': '952-908-9099', 'Kate': '402-214-6709', 'Tom': '651-432-9090'}

## sets
my_set = {1,1,2,3,3,4,5,4,5,6,6}

## tuples
my_tuple = ('a', 'b', 'c')

# There are also built-in functions to create these types of data structures
my_list = list([1,2,3,4,5])
phone_numbers = dict([('John', '952-908-9099'), ('Kate', '402-214-6709'), ('Tom', '651-432-9090')])
my_set = set('aabbccdefghjkl')
my_tuple = tuple(['a', 'b'])

# You can also creat nested data structures:
my_2d_list = [[1,2,3], [4,5,6], [7,8,9]]

nested_dict = {'John': {'phone number': '952-908-9099', 'address': '123 Main Street'},
               'Kate': {'phone number': '402-214-6709', 'address': '450 Birch Avenue'},
               'Tom': {'phone number': '651-432-9090', 'address': '900 1st Avenue S'}}

########## Homework #1 time! ###############
## Initialize three variables with integers and store them in a list
########## End homework #1 time! ##############

### Loops
## while loop -- perform an operation while a condition is set
n_apples = 0
while n_apples < 5:
    print('Current number of apples: %d' % n_apples)
    n_apples += 1
    
# While loops can help us iterate through *some* iterables, e.g. lists or tuples
my_arr = [1, 2, 3, 4, 5]
idx = 0
while idx < len(my_arr):
    print(my_arr[idx])
    idx += 1
    
possible_answers = ('Yes', 'No')
answer = input('Enter "yes" or "no": ')
while answer not in possible_answers:
    answer = input('"%s" is not in %s, please try again: ' % (answer, possible_answers))
    
## for loop -- iterate through an iterable
numbers = [9,8,2,43,4,31,1,24,4]
for number in numbers:
    print('Current number: %d' % number)

# With `enumerate`, the for loop becomes just as powerful as the while loop
for idx, elem in enumerate(numbers):
    print('Index', idx)
    print('Element at index %d is %d' %(idx, numbers[idx]))
    print('Elem value', elem, '\n')
    
# Print out ten numbers, starting at 0
for i in range(10):
    print(i)

### Control statements
## if statements:
# if <some condition is true>:
#    do something

current_balance = 8
cost_of_burrito = 7
if current_balance >= cost_of_burrito:
    print('You can afford a %d-dollar burrito since you have $%.2f' % (cost_of_burrito, current_balance))
    
store_is_open = True
if current_balance >= cost_of_burrito and store_is_open:
    print('Go buy that burrito!')
    
current_balance -= cost_of_burrito

# if-else statements
cost_of_guac = 1.75
if current_balance >= cost_of_guac:
    can_afford_guac = True
    print('You can afford guac')
else:
    can_afford_guac = False
    print('You don\'t have enough money for guacamole. Sorry.')

want_guac = False

# if-elif-else statements
if can_afford_guac and want_guac:
    print('You want guac? You got it...')
elif can_afford_guac and not want_guac:
    print('You can afford guacamole, but you don\'t want it')
else:
    print('Maybe just go without extras today, '
          'since you don\'t want guacamole or can\'t afford it...')
    
# ternary conditional operator
bought_burrito = 'yes' if cost_of_burrito <= current_balance else 'no'

print('true' if False else 'false')

# basic if-else 
if True:
    print(True)
else:
    print(False)

### Functions
## Parameter-less
def print_the_same_thing():
    print('This is the same thing')

# A lot of times, parameter-less functions are used for the main() function
x = 10
def main():
    print(x)

main()

## With parameters
def print_list(my_list):
    for elem in my_list:
        print(elem)

def add(x, y):
    sum_xy = x + y
    print('%d plus %d equals %d' % (x,y,sum_xy))
    
# We can add a return statement to 'get back' whatever variables/values we want
# from the function's operations
def add(x, y):
    sum_xy = x + y
    return sum_xy    
        
def left_less_than_right(x, y):
    return True if x < y else False
    
# with parameter defaults
def division(x, y, floor=False):
    if floor:
        div = x // y
    else:
        div = x / y
    return div

def custom_print(my_list, only_evens=True):
    if only_evens:
        for elem in my_list:
            if elem % 2 == 0:
                print(elem)
    else:
        for elem in my_list:
            print(elem)

########## Homework #2 time! ###############
## Define a function that returns the largest odd number in a given list
########## End homework #2 time! ##############

## Useful Python built-in functions
# max and min
arr = [1,2,3,4,3,2,1,4,5,0]
max_value = max(arr)
min_value = min(arr)

print('Maximum value is', max_value)
print('Minimum value is', min_value)
            
string_arr = ['a', 'b', 'c', 'c', 'd', 'a']
print('The maximum element in %s by ASCII value is "%s"' % (string_arr, max(string_arr)))
            
# range
for x in range(1, 11):
    print(x)
    
assert range(1, 11) == [1,2,3,4,5,6,7,8,9,10], "That range isn't a list"
            
# map
all_strings = ['1', '2', '2.56', '2.0', '3.10100']
now_floats = map(float, all_strings)
now_strings = map(str, now_floats)    
    
# lambdas are custom functions that are useful for on-the-fly operations
subtract = lambda x, y: x - y
print(subtract(5, 10))

all_strings_plus_something = map(lambda x: x + 15, now_floats)

# zip is useful for pairing up disparate lists into one object
names = ['Jake', 'John', 'Sally', 'Susie']
ages = [9, 10, 8, 13]

names_ages = zip(names, ages)

# type
print(type(5.556))
print(type('this is a string'))
print(type(1))
print(type({'a': 1, 'b': 2}))

### Reading and writing to files
## Reading files
with open('/Users/jacobmason/Desktop/dictionary.txt', 'r', encoding = 'iso-8859-1') as f:
    for line in f:
        print(line)
        
## Writing to files
with open('/Users/jacobmason/Desktop/new_file.txt', 'w+') as f:
    for word in ['these', 'are', 'some', 'words']:
        f.write('%s\n' % word)

### Basic debugging
## EOL while scanning string literal
a = 'this is an unfinished string'

## Syntax error
x = 5
print x

if x < 5
    print(x)

# bad indentation block
if x < 5:
print(x)

### Generators
# Generators are useful for dealing with 'streaming' data, i.e. large 
# amounts of data that can be 'streamed' in and out of your program. Generators
# are also helpful when dealing with large amounts of data. For example, storing
# 100 million integers in a list in memory in Python takes up at least 2.8gb of memory.
# A generator would be a better choice here.
def basic_range(n):
    i = 0
    while i < n:
        yield i
        i += 1
    
### Comprehensions
# Comprehensions are concise, intuitive, and Pythonic ways of creating certain
# containers, e.g. lists, dicts, sets, and generators

## list comprehension
arr = [num for num in range(10)]
evens = [num for num in range(10) if num % 2 == 0]

urls = ['www.google.com', 'www.google.ca', 'www.google.in', 'www.yahoo.com',
        'www.gmail.com', 'www.google.de']
        
only_google = [url for url in urls if 'google' in url]

## set comp
arr = [1,1,2,3,2,3,2,1,2,3,2,1,2,3]
uniques = {x for x in arr}

## dictionary comprehension
squared_dict = {n: n**2 for n in range(10)}
evens_squared_dict = {n: n**2 for n in range(10) if n % 2 == 0}

########## Homework #3 time! ###############
## Initialize a list containing numbers 1-10 using a comprehension
########## End homework #3 time! ##############

### Classes
## From classes comes Python's object-oriented nature. Think of a class as 
## a logical grouping of functions and data. Classes are like blueprints - they
## contain all the necessary information needed to build an object.

## Let's start with an example:
class Square:
    def __init__(self, x):
        self.x = x
    def area(self):
        return self.x ** 2
    def perimiter(self):
        return self.x * 4

class Account:
    def __init__(self, name, initial_deposit):
        self.name = name
        self.balance = initial_deposit
    def withdraw(self, amount):
        if amount > self.balance:
            raise Exception('Amount is greater than current balance.')
        print('Withdrawing %d...' % amount)
        self.balance -= amount
    def deposit(self, amount):
        print('Depositing %d...' % amount)
        self.balance += amount
    def get_balance(self):
        print('Current balance is %.2f.' % balance)

### Importing modules
## Modules (a.k.a. "packages" or "libraries") enhance your Python code
## and provide robust ways of dealing with common problems

## Some very useful modules from the Python Standard Library include
# itertools - functions for efficient looping
import itertools
options = ['A', 'B', 'C', 'D']
combos = itertools.combinations(options, 2)

# math - a solid library, but numpy is the industry standard
import math
print('The square root of 9 is: %d' % math.sqrt(9))

# statistics - another solid library, but numpy is preferred, esp. in data science
import statistics
arr = [1,2,3,4,5,4,3,4,5,4,2]

mean = statistics.mean(arr)
median = statistics.median(arr)
sd = statistics.stdev(arr)

# random - useful for creating random sequences, selecting random elements, etc.
import random
random_choices = [random.choice(('A', 'B')) for _ in range(5)]

# urllib - a great library that enables easy connecting to/interfacing with webpages
import urllib

url = 'http://www.kdnuggets.com'
opened = urllib.request.urlopen(url)
html = opened.read()

# There are many, many more useful modules in the Python Standard Library. including 
# parsers, command-line interface tools, networking tools, etc.
# I would recommend checking them out: https://docs.python.org/3.5/library/
