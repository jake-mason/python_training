# This python file is for scripts demonstrating numpy, pandas, and matplotlib
import numpy as np

# 1. Numpy Arrays
# numpy arrays can be created from python lists, but look a little different
#  in an interactive shell
numpyArray = np.array([8,6,7,5,3,0,9])
pyList = list([8,6,7,5,3,0,9])

# inspect the types of these lists, and look at how they print out
print("type of pyList:", type(pyList))
print("  pyList:",pyList)
print("type of numpyArray:",type(numpyArray))
print("  numpyArray:",numpyArray)

# Demonstrate slicing behavior
print("slicing a numpy array still works as you'd expect")
numpyArray[0:4]
numpyArray[1::2]


# Demonstrate built-ins like sum,min,max
print("basic python builtin functions also work the same")
sum(numpyArray)
min(numpyArray)
max(numpyArray)

# Show off numpy's versions
print("but numpy includes its own versions of these functions")
np.sum(numpyArray)
np.min(numpyArray)
np.max(numpyArray)

# 2. Timeit and speed discussion
# Show speed comparison to basic python version
print("so why should we care about numpy when there are built-ins? SPEED!")
from timeit import timeit
# timeit is a library that lets you time snippets of code
# timeit("statementYouWantToTime",
#         setup="any setup code you don't want to count against speed",
#         number = numberOfTimesToRun)

standardTime = timeit("sum(timeArray)",
                      setup="timeArray=range(100000)",
                      number = 1000
                     )

numpyTime = timeit("np.sum(timeArray)",
                   setup="import numpy as np; timeArray=np.arange(100000)",
                   number = 1000
                   )

print("standard list sum took:",standardTime)
print("numpy list sum took   :",numpyTime)
print("numpy was faster by a factor of", standardTime/numpyTime)

# 3. Matrices
# Numpy Matrices.
print("numpy also makes it easier to deal with matrices")
print("  this matters, because matrices are fundamental to data science")
print("  Don't worry though, you'll get to mostly ignore them soon!")
print("we can create an identity matrix")
identity = np.eye(3)
print(identity)

print("we can multiply all the elements of a matrix by a number")
scalingMatrix = 3*identity

print("we can take a list of numbers, and 'reshape' it into a matrix")
increasingMatrix = np.arange(9).reshape((3,3))
print( increasingMatrix)

print("we can add matrices together")
addedMatrix = increasingMatrix + scalingMatrix
print(addedMatrix)

print("finally, we can multiply matrices together")
multipliedMatrix = np.matmul(addedMatrix,increasingMatrix)
print(multipliedMatrix)

print("and thankfully, all of that computation takes place at fortran speed!")

# 4. Random Functions

# numpy comes with a number of functions for generating random numbers

# set a seed to the random number generator to guarantee the same random numbers
#   on each run
np.random.seed(0)
gaussianGenerator = np.random.normal
print("a random number from N(0,1):", gaussianGenerator(loc = 0, scale = 1))

print("we can also generate MILLIONS of variables")
nSamples = 2000000
millionSamples = gaussianGenerator(loc=0,scale=1.0,size=nSamples)
print( np.mean(millionSamples))
print( np.std(millionSamples))

print("let's check out another timing comparison")
standardAvg = timeit("sum(millionSamples)/nSamples",
                      setup="import numpy as np;np.random.seed(0);\
                      nSamples= 2000000;\
           millionSamples = np.random.normal(loc=0,scale=1.0,size=nSamples)",
                      number = 100
                     )

numpyAvg = timeit("np.mean(millionSamples)",
                      setup="""import numpy as np;np.random.seed(0);\
                      nSamples= 2000000;\
           millionSamples = np.random.normal(loc=0,scale=1.0,size=nSamples)""",
                      number = 100
                     )

print("standard list sum took:",standardAvg)
print("numpy list sum took   :",numpyAvg)
print("numpy was faster by a factor of", standardAvg/numpyAvg)

# Homework: Investigate the options in numpy.random with help(numpy.random)
#   set a seed, choose a new distribution, and generate a few million samples
#   set the seed again, and generate the numbers again. Do you get the same result?

# introduce pandas
import pandas as pd

# make a series from numpy random functions, specifying the name 'normal'
randomSeries = pd.Series(millionSamples,name='normal')
print(randomSeries.head())

# Describe is an easy way to see the five number summary for a pandas Series
print(randomSeries.describe())

# let's make another random series with the standard cauchy distribution
millionCauchySamples = np.random.standard_cauchy(size=nSamples)
cauchySeries = pd.Series(millionCauchySamples,name='cauchy')
print(cauchySeries.describe())

# You can also take functions, and apply them to Series. 
print(abs(cauchySeries).describe())
print(abs(randomSeries).describe())

# Just calculating these transformations doesn't overwrite the underlying data
print(cauchySeries.describe()) # Note that this still has negatives!

     
# We can also evaluate logical statements over a series
# Compare cauchySeries element by element to 0
cauchyIsPositive = cauchySeries > 0 
print(cauchyIsPositive.head()) 

# Compare the two series element by element
cauchyExceedsNormal = cauchySeries > randomSeries
print(cauchyExceedsNormal.head())

# You can use logical series (like those generated from comparisons) to take
#  portions of a series

# Use the series we defined earlier to grab all the positive cauchy numbers
positiveCauchySeries = cauchySeries[cauchyIsPositive]
print(positiveCauchySeries.head())
print(positiveCauchySeries.describe())

# Define a boolean series on the fly to grab normal distribution outliers
#   defined by >2 standard deviations from the mean
normalOutliers  = randomSeries[abs(randomSeries)>2]
print(normalOutliers.head())
print(normalOutliers.describe())
     
# we can take pandas series, and organize them into Dataframes
# first merge the series
randomFrame = pd.concat([randomSeries,cauchySeries],axis=1)

# just like series, randomFrames have a describe method for summary statistics
print(randomFrame.describe())

# We can inspect particular columns using dataFrame[columnName]
print(randomFrame['cauchy'].describe())

# Doing this returns a column
print(type(randomFrame['cauchy']))

# We can select rows from a dataframe with a boolean series, just like we did
#   earlier with a series
biggerCauchyFrame = randomFrame[cauchyExceedsNormal]
print(biggerCauchyFrame.describe())

# We can create and store data in a new column by specifying a name for the 
#   column, and appropriate values to store. For example, we could define a new
#   column containing cauchy+normal data
randomFrame['sum'] = randomFrame['cauchy'] + randomFrame['normal']
print(randomFrame.describe())
# Notice that randomFrame['sum'] wasn't already a series in randomFrame, but
#   cauchy and gaussian WERE.


# If we want to take a dataframe, and return a new dataframe with some selection
#   of columns, we can take a subset using two brackets instead of one
randomFrame[['cauchy','sum']].describe()

# Now to address the fundamental problem of computing, how do I get data into
#   my program, and get it back out later?
# Check the help file to see some of the read options
help(pd)
# BUT, there are many more! Including pd.read_xlsx, read_sas, read_sql, 
#   read_stata, etc

# show how to load in iris data from csv
irisDataFrame = pd.read_csv("C:/Users/kelly634/Code/python_training/iris.csv")
# It's that simple!

# But we can make it more complicated, and manually specify that the header 
#  is on line 0, the column names are what they are, the delimiter is ',' for
#  a csv, and then manually specify dtypes, but pandas usually does a pretty
#  good job of reading your file and figuring things out from looking at a few
#  rows.
pd.read_csv("C:/Users/kelly634/Code/python_training/iris.csv",
            header=0,
            names=['sepal_length', 'sepal_width','petal_length',
            'petal_width','species'],
            delimiter=",",
            dtype={
            'sepal_length':np.float,
            'sepal_width':np.float,
            'petal_length':np.float,
            'petal_width':np.float,
            'species':str}
            )

# If we load a file without knowing what the headers are, use columns
print(irisDataFrame.columns)

# We can also assign values to the columns field, if we don't like the '_' char
irisDataFrame.columns = [
  'Sepal Length',
  'Sepal Width',
  'Petal Length',
  'Petal Width',
  'Species'
  ]
print(irisDataFrame.describe())

# But wait, there's a species column and it's not showing up in describe. Why?
print(irisDataFrame.dtypes)   # non-numeric data won't show up in describe

# What if we want to know something about non-numeric columns?
#  I recommend value_counts of the non-numeric series
irisDataFrame['Species'].value_counts()

# We can assign values to a subset of the dataframe, and since this data is too
#   nicely formatted to show off functions like isnull and isnotnull, make a 
#   new columns with some nulls
irisDataFrame['Sometimes Null'] = irisDataFrame.apply(
        lambda aRow: gaussianGenerator(loc=0,scale=1.0) if aRow['Sepal Width']>aRow['Petal Length'] else None,
        axis = 1
    )

print(irisDataFrame.describe()) # notice that 'Sometimes Null' only has count=50

# We can use the pandas isnull function to figure out where the nulls are
irisDataFrame['Sometimes Null'].isnull().head() # note it's a boolean series

# We can use the boolean series to find the Null values
hadNull = irisDataFrame[irisDataFrame['Sometimes Null'].isnull()]

# Show off pandas built-in plotting functions
# We can make histograms with the 'hist' method, charting frequencies
histograms = irisDataFrame.hist(column='Sepal Width',by='Species',
                                sharex = True,sharey=True)


# For more info on histogram options, consult help(pd.DataFrame.hist)
randomFrame.sample(n=100).plot(kind='scatter', x='normal',y='sum',ylim=(-4,4))


from sklearn.linear_model import LinearRegression

linearRegressor = LinearRegression(fit_intercept=True, normalize=True, n_jobs=-1)

# fit a linear regression model to predict sum, given cauchy
linearRegressor.fit(randomFrame[['cauchy']],randomFrame['sum'])
# 

regressedSum = linearRegressor.predict(randomFrame[['cauchy']])

from sklearn.metrics import r2_score, mean_squared_error, median_absolute_error
print( r2_score(regressedSum, randomFrame[['sum']]))
print( mean_squared_error(regressedSum, randomFrame[['sum']]))
print( median_absolute_error(regressedSum, randomFrame[['sum']]))

# Note: SKLearnModels are objects, that offer a fit method. 
# Objects for classification and regression offer a predict method as well

randomFrame['regressed_sum'] = regressedSum
randomFrame['residuals']  = randomFrame['regressed_sum'] - randomFrame['cauchy']

# Homework: 
#    a) repeat this regression exercise, using the gaussian distributed 
#         variable to predict y instead of the cauchy distributed variable

from sklearn.neighbors import KNeighborsClassifier
from sklearn.metrics import log_loss, accuracy_score, auc

neighborClassifier = KNeighborsClassifier(n_neighbors=20,n_jobs=-1)
neighborClassifier.fit(irisDataFrame.drop(['Species','Sometimes Null'],axis=1),irisDataFrame[['Species']])

neighborClassPrediction = neighborClassifier.predict(irisDataFrame.drop(['Species','Sometimes Null'],axis=1))
speciesProbabilities = neighborClassifier.predict_proba(irisDataFrame.drop(['Species','Sometimes Null'],axis=1))

print( log_loss(irisDataFrame['Species'],speciesProbabilities))
print( accuracy_score(neighborClassPrediction, irisDataFrame[['Species']]))

# Homework:
#   a) change the number of neighbors, and reduce log loss/increase accuracy as MUCH as possible
#   b) why does a result in a bad model?


